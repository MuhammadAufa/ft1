//Seringkali kita melihat data yang dienkripsi terhadap informasi yang bersifat rahasia. Beragam metode enkripsi telah dikembangkan hingga saat ini. Kali ini kalian akan ditugaskan untuk membuat metode enkripsi sendiri agar informasi yang diinput menjadi aman, dengan menggunakan original alfabet yang dirotasi pada enkripsi tersebut.
//
//Constraint :
//-    Original Alfabet : abcdefghijklmnopqrstuvwxyz
//-    Semua enkripsi hanya menggunakan huruf kecil
//-    Spasi/karakter spesial tidak dianggap
//
//Input :
//string : mengandung data yang belum dienkripsi
//n : jumlah huruf yang digunakan untuk merotasi original alfabet (0 <= n <= 100)
//
//Example
//string : ba ca
//n : 3
//
//Output
//Original Alfabet : abcdefghijklmnopqrstuvwxyz
//Alfabet yang dirotasi : defghijklmnopqrstuvwxyzabc
//Hasil enkripsi : edfd


package FT1;

import java.util.Scanner;

public class no8 {

	public static void main(String[] args) {
		Scanner tes = new Scanner(System.in);
		
		System.out.println("Masukkan string : ");
		String data = tes.nextLine();
		
		System.out.println("Masukkan jumlah rotasi : ");
		int n = tes.nextInt();
		
		String []alpha = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
		String []kunci = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
		String []kata = data.toLowerCase().split(" ");
		
		for (int i = 0; i < n; i++) {
			String wadah = kunci[0];
			for (i = 0; i < kunci.length - 1; i++) {
				kunci[i] = kunci[i + 1];
				
			}
			kunci[kunci.length - 1] = wadah;

		}
		for (String x : kunci) {
			System.out.print(x);
			
		}
		
		for (int i = 0; i < kata.length-1; i++) {
			for(int j = 0; j < alpha.length; j++) {
				if (kata[i].equalsIgnoreCase(data[j])) {
					System.out.println(kunci[j]);
				}
			}
		}
		
		

	}

}
