//Berapa banyak angka fibonacci selain nol dibawah x yang merupakan angka genap
//
//Input x = ?
//Output: Sebanyak ...

package FT1;

import java.util.Scanner;

public class no9 {

	public static void main(String[] args) {
		Scanner tes = new Scanner(System.in);
		
		System.out.println("Input x = ");
		int n = tes.nextInt();
		
		int x = 1;
		int y = 0;
		int fb = 1;
		int z = 0;

		for (int i = 1; i < n; i++) {
			x = y;
			y = fb;
			System.out.print(fb + " ");
			fb = x + y;
			if(fb < n && fb % 2 == 0 && fb > 0) {
				z++;
			}
		
		}
		System.out.println();
		System.out.println("Sebanyak "+z);
	}

}
