//Buatlah deret angka yang terbentuk dari penjumlahan deret bilangan kelipatan 3 dikurang 1 dan deret bilangan kelipatan 4 diibagi(/) 2. Angka pada index ganjil dari kedua deret bilangan tersebut saling dijumlahkan. Dan angka pada index genap dari kedua deret bilangan tersebut juga saling dijumlahkan. Index dimulai dari angka 0.
//Input : Panjang array/panjang deret
//Contoh : Dibawah ini hanya sekedar contoh yang menggunakan deret genap dan ganjil
//
//Input panjang deret : 5
//Deret genap : 0 2 4 6 8
//Deret ganjil : 1 3 5 7 9
//0 + 1 ; 2 + 3 ; 4 + 5 ; 6 + 7 ; 8 + 9
//
//Output : 1, 5, 9, 13, 17


package FT1;

import java.util.Scanner;

public class no1 {

	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		
		System.out.print("Panjang deret :");
		int p = kb.nextInt();

		int[] aTiga = new int[p+1];
		int[] aEmpat = new int[p+1];
		int[] akhir = new int [p+1];

		int y = 2;
		for(int i = 1; i <= p; i++) {
			aTiga[i] = y;
			y = (y + 3) ;
			//System.out.print(" " + aGenap[i]);
		}

		int x = 0;
		int x2 = 0;
		for(int j = 1; j <= p; j++) {
			aEmpat[j] = x2;
			x += 4;
			x2 = x / 2;
			//System.out.print(" " + aGanjil[j]);
		}

		System.out.print("deret kelipatan 3 kurang 1 :");
		for (int i = 1; i<aTiga.length; i++) {
			System.out.print(aTiga[i] + " ");
		}
		System.out.println();
		
		System.out.print("deret kelipatan 4 bagi 2 :");
		for (int i = 1; i<aEmpat.length; i++) {
			System.out.print(aEmpat[i] + " ");
		}
		System.out.println();
		
		System.out.print("sum index genap : ");
		

		int jGenap = 0;
		for (int i = 1; i < p; i++) {
			if (i % 2 == 0) {
				System.out.print(aTiga[i] + " + " + aEmpat[i] + " ");
				jGenap = jGenap + (aTiga[i] + aEmpat[i]);
			}
		}
		System.out.print(" = " + jGenap);
		
		System.out.println();
		
		System.out.print("sum index ganjil : ");
		
		int jGanjil = 0;
		for (int i = 0; i < p; i++) {
			if (i % 2 != 0) {
				System.out.print(aTiga[i] + " + " + aEmpat[i] + " ");
				jGanjil = jGanjil + (aTiga[i] + aEmpat[i]);
			}
			// System.out.print(ganjil+" ");
		}
		
		
		System.out.print(" = " + jGanjil);
		
		System.out.println();
		
		for(int i = 0; i < p; i++) {
			if (i%2 == 0) {
				akhir[i] = aTiga[i] + aEmpat[i];
				System.out.print(akhir[i] + " ");
			}
		}
		System.out.println();
		for(int i = 0; i < p; i++) {
			if (i%2 != 0) {
				akhir[i] = aTiga[i] + aEmpat[i];
				System.out.print(akhir[i] + " ");
			}
		}

		

		kb.close();
	}

}

