//Dengan hanya menggunakan logic, ubah format jam dari 24H ke 12H dan juga sebaliknya
//
//contoh:
//input: 12:35 AM
//output: 00:35
//
//input: 19:30
//output: 07:30 PM
//
//input: 09:05
//output: 09:05 AM
//
//input: 11:30 PM
//output: 23:30
//
//NB: perbedaan format 12H dan 24H ada pada jam 00 tengah malam (jam 00 adalah jam 12 AM, lihat contoh 1), selebihnya tinggal menambahkan PM antara jam 12:00 - 23:59 dan AM antara jam 00:00 - 11:59

package FT1;

import java.util.Scanner;

public class no4 {

	public static void main(String[] args) {
		Scanner tes = new Scanner(System.in);
		System.out.println("Masukkan waktu : ");
		String amPm = tes.nextLine();
		
		int hh = Integer.parseInt(amPm.substring(0,2));
		//System.out.println(hh);
		
		//cek am or pm
		if(amPm.length() == 5) {
			if (hh == 12) {
				System.out.print(hh);
				System.out.print(amPm.substring(2,5));
				System.out.println(" PM");
			}
			else if (hh > 12) {
				System.out.print(hh - 12);
				System.out.print(amPm.substring(2,5));
				System.out.println(" PM");
			}
			else {
				if(hh<10) {
					System.out.println("0"+hh);
				} else {
				System.out.print(hh);
				System.out.print(amPm.substring(2,5));
				System.out.println(" AM");
			}
			}
		}
		else {
		
		if(amPm.charAt(6) == 'A') {
			//am
			if(hh==12) {
				//cetak jam
				System.out.print("00");
				//cetak mm, ss
				System.out.print(amPm.substring(2,6));
			}
			else {
				System.out.println(amPm.substring(0,6));
			}
		}else {
			//pm
			if(hh == 12) {
				//jam
				System.out.print("12");
				//mm, ss
				System.out.print(amPm.substring(2,6));
			}
			else {
				hh = hh + 12;
				System.out.print(hh);
				System.out.print(amPm.substring(2,6));
			}
		}

	}

}
}
