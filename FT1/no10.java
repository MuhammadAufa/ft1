package FT1;

import java.util.Scanner;

public class no10 {

	public static void main(String[] args) {
		Scanner tes = new Scanner(System.in);

		System.out.print("Input : ");
		String kalS = tes.nextLine();

		String kal = kalS.replaceAll(" ", "");
		String[] arr = kalS.replaceAll(" ", "").split("");

		char[] aChar = new char[arr.length];
		char[] filter = new char[aChar.length];
		int posisi = 0;

		String vokal = "";
		String konsonan = "";

		// isi array char
		for (int i = 0; i < aChar.length; i++) {
			aChar[i] = arr[i].toLowerCase().charAt(0);
		}

		for (int i = 0; i < aChar.length; i++) {
			for (int j = 0; j < aChar.length - 1; j++) {
				char wadah = aChar[j];
				if (aChar[j] > aChar[j + 1]) {
					aChar[j] = aChar[j + 1];
					aChar[j + 1] = wadah;
				}

			}
		}
		for (int i = 0; i < aChar.length; i++) {
			boolean kembar = false;
			for (int j = 0; j < filter.length; j++) {
				if (aChar[i].equals(filter[j])) {
					kembar = true;
					break;
				}
			}
			if (kembar == false) {
				filter[posisi] = aChar[i];
				posisi++;
			}

		}
		for (char c : aChar) {
			System.out.print(c + " ");
		}

		for (int i = 0; i < kal.length(); i++) {
			if ((aChar[i]) == 'a' || (aChar[i]) == 'i' || (aChar[i]) == 'u' || (aChar[i]) == 'e' || (aChar[i]) == 'o') {
				//if(aChar[i] != ) {
				vokal = vokal + aChar[i];
				//}
			} else {
				konsonan = konsonan + aChar[i];
			}
		}
		System.out.println();
		System.out.println("Vokal : " + vokal.toLowerCase());
		System.out.println("Konsonan : " + konsonan.toLowerCase());
		tes.close();
	}

}
