//Anda akan menggunting-gunting tali sepanjang z meter menjadi beberapa buah tali sepanjang x meter. 
//Berapa kali sedikitnya anda akan menggunting tali tersebut ?
//Contoh: z = 4, x = 1
//Cukup menggunting 2x (pertama, tali 4m dibagi 2 sama rata, akan didapatkan masing-masing 2m. 
//Kemudian kedua tali 2m itu dipotong bersama sama rata, akan dihasilkan 4 tali masing-masing panjang 1m)


package FT1;

import java.util.Scanner;

public class no7 {

	public static void main(String[] args) {
		Scanner tes = new Scanner(System.in);
		
		System.out.print("Panjang tali : ");
		int pTali = tes.nextInt();
		
		System.out.print("Potong menjadi : ");
		int pPotong = tes.nextInt();
		
		int gunting = 0;
		
		if(pTali%2 != 0 && pPotong%2 !=0) {
			pTali = pTali - 1;
			gunting = pTali / pPotong;
		}
		
		/*if(pTali/2 == pPotong*2) {
			gunting = 2;
		}*/
		
		else if(pTali/pPotong == 2) {
			gunting = 1;
		}
		else {
			gunting = pTali / pPotong;
		}
		
		System.out.print("Cukup menggunting " + gunting + "x");
	
		

	}

}
